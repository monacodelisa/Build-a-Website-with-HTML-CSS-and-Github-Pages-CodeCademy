<h1> Build a Website with HTML, CSS and Github Pages</h1><h3>Skill Path - CodeCademy</h3>

<p> Here I am adding the projects that I completed <br> as part of my codecademy course - Jun 16, 2020</p>

<h2><a href="https://monacodelisa.github.io/Colmar-Academy/" target="_blank">9. Colmar Academy - moved to a seperate Repo</a></h2>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/7-pt1-Tea-Cozy/">7.1. Tea Cozy</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/7-Flexbox-To-Do%20App/" target="_blank">7. Flexbox To Do App</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/6-Tsunami-Coffee/" target="_blank">6. Tsunami Coffee</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/5-pt1-Broadway/" target="_blank">5.1. Broadway</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/5-The-Box-Model-Davie's-Burgers/" target="_blank">5. The Box Model Davie's Burgers</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/4-pt1-Typography/" target="_blank">4.1. Typography</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/4-Paint-Store/" target="_blank">4. Paint Store</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/3-Dasmoto's-Arts-&-Crafts/" target="_blank">3. Dasmoto's Arts & Crafts</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/2-Olivia-Woodruff-Portfolio/" target="_blank">2. Olivia Woodruff Portfolio</a></h3>
<h3><a href="https://monacodelisa.github.io/Build-a-Website-with-HTML-CSS-and-Github-Pages-CodeCademy/1-Fashion-Blog-my-ver-responsive/" target="_blank">1. Fashion Blog - my version, responsive</a></h3>

<a href='https://ko-fi.com/monacodelisa' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

